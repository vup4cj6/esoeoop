public class Triangle extends Shape{
    public Triangle(double length){
        super(length);
    }
    
        @Override
        public void setLength(double length) {
            this.length=length;
    
        }
    
        @Override
        public double getArea() {
            return Math.round(this.length*this.length/4*Math.pow(3,0.5)*100.0)/100.0;
        }
    
        @Override
        public double getPerimeter() {
            return this.length*3;
        }
        

}