public class Square extends Shape{

public Square(double length){
    super(length);
}

    @Override
    public void setLength(double length) {
        this.length=length;

    }

    @Override
    public double getArea() {
        return Math.round(this.length*this.length*100.0)/100.0;
    }

    @Override
    public double getPerimeter() {
        return this.length*4;
    }
    
}