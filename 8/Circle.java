public class Circle extends Shape{
    public Circle(double length){
        super(length);
    }
    
        @Override
        public void setLength(double length) {
            this.length=length;
    
        }
    
        @Override
        public double getArea() {
            return Math.round(this.length*this.length/4*Math.PI*100.0)/100.0;
        }
    
        @Override
        public double getPerimeter() {
            return Math.round(this.length*Math.PI*100.0)/100.0;
        }
        
    
}