public class SentenceProcessor {
    public String removeDuplicatedWords(String A) {
        String a = "";
        String[] b = A.split(" ");
        for (int i = 0, l = b.length; i < l; i++) {
            int t = 0;
            for (int j = 0; j < i; j++) {
                if (b[i].equals(b[j])) {
                    t = 1;
                }
            }
            if (t == 0 && i == 0) {
                a = b[i];
            }
            if (t == 0 && i != 0) {
                a = a + " " + b[i];
            }
        }
        return a;
    }
     public String replaceWord(String C,String B,String A) {
        String r = "";
        String[] b = A.split(" ");
        for (int i = 0, l = b.length; i < l; i++) {
            int t = 0;
            
                if (b[i].equals(C)) {
                    t = 1;
                }
            
            if (t == 0 && i == 0) {
                r = b[i];
            }
            if (t == 0 && i != 0) {
                r = r + " " + b[i];
            }
            if (t == 1 && i == 0) {
                r = B;
            }
            if (t == 1 && i != 0) {
                r = r + " " + B;
            }
        }

        return r;
     }
}