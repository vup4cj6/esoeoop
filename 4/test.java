public class test {

    public static void main(String[] args) {
        
        SentenceProcessor sp = new SentenceProcessor();

System.out.println(sp.removeDuplicatedWords("Hello Hello World I love love the World I lovelove the World"));
System.out.println(sp.removeDuplicatedWords("Buffalo buffalo Buffalo buffalo buffalo buffalo Buffalo buffalo"));
System.out.println(sp.removeDuplicatedWords("a a la a la carte A la La carte Carte A a la la"));
System.out.println(sp.replaceWord("major", "minor", "The major problem is how to sing in A major"));
System.out.println(sp.replaceWord("on", "off", "Turn on the television I want to keep the television on"));
System.out.println(sp.replaceWord("love", "hate", "I love the World I lovelove the Love"));
    }

}