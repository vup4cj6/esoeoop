public class IsLeapYear {
    public static boolean determine(int year) {
        
        boolean a=((year%4==0)&&(year%100!=0))||(year%400==0);

        return a;
    }
}