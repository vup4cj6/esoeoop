public class ATM_Exception extends Exception {
    public static final long serialVersionUID = 1L;
    public ExceptionTYPE excptionCondition;

    public enum ExceptionTYPE {
        BALANCE_NOT_ENOUGH, AMOUNT_INVALID;
    }

    public ATM_Exception(ExceptionTYPE excptionCondition) {
        this.excptionCondition = excptionCondition;
    }

    @Override
    public String getMessage() {
        switch (excptionCondition) {
            case BALANCE_NOT_ENOUGH:
                return "BALANCE_NOT_ENOUGH";
            case AMOUNT_INVALID:
                return "AMOUNT_INVALID";

            default:
                return "000";
        }
    }
}