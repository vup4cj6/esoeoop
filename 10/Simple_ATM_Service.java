public class Simple_ATM_Service implements ATM_Service {
    public Simple_ATM_Service(){
        
    }
    @Override
    public boolean isValidAmount(int money) throws ATM_Exception{
        if(money%1000!=0){
            throw new ATM_Exception(ATM_Exception.ExceptionTYPE.AMOUNT_INVALID);
        }
        return true;

    }
    @Override
    public void withdraw(Account account, int money){
        try{
            this.checkBalance(account,money);
            this.isValidAmount(money);
            account.setBalance(account.getBalance()-money);
            System.out.println("updated balance : "+account.getBalance());
        }catch(ATM_Exception e){
            System.out.println(e.getMessage());
            System.out.println("updated balance : "+account.getBalance());
        }
    }

    @Override
    public boolean checkBalance(Account account, int money) throws ATM_Exception {
        if(account.getBalance() < money) {
            throw new ATM_Exception(ATM_Exception.ExceptionTYPE.BALANCE_NOT_ENOUGH);
        }
        return true;
    }

    
}