/**
 * 點披薩
 * 最多三個最少一個
 * 可計算總價
 */

public class PizzaOrder {
    private int numberPizzas;
    private Pizza pizza1;
    private Pizza pizza2;
    private Pizza pizza3;

    public PizzaOrder(){
    }
    /**
     * 設定訂購數量
     * 只能介於一至三
     * @param n
     * @return
     */
    public boolean setNumberPizzas(int n){
        if(n<1||n>3){
            return false;
        }
        else{
            this.numberPizzas=n;
            return true;
        }
    }
    /**
     * 設定第一個披薩
     * @param one
     */
    public void setPizza1(Pizza one){
        pizza1=one;
    }
    /**
     * 設定第二個披薩
     * @param two
     */
    public void setPizza2(Pizza two){
        pizza2=two;
    }
    /**
     * 設定第三個披薩
     * @param three
     */
    public void setPizza3(Pizza three){
        pizza3=three;
    }
    /**
     * 計算總價
     * @return 總價
     */
    public double calcTotal(){
        
        switch(this.numberPizzas){
            case 1:
                return pizza1.calcCost();
            case 2:
                return pizza1.calcCost()+pizza2.calcCost();
            case 3:
                return pizza1.calcCost()+pizza2.calcCost()+pizza3.calcCost();
            default:
                return 0;
        }
    }
}