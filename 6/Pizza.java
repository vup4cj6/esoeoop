public class Pizza {
    private String size;
    private int cheese;
    private int pepperoni;
    private int ham;

    public Pizza() {
        this.size = "small";
        this.cheese = 1;
        this.pepperoni = 1;
        this.ham = 1;
    }

    public Pizza(String A, int B, int C, int D) {
        this.size = A;
        this.cheese = B;
        this.pepperoni = C;
        this.ham = D;
    }

    public void setSize(String A) {
        this.size = A;
    }

    public void setNumberOfCheese(int A) {
        this.cheese = A;
    }

    public void setNumberOfPepperoni(int A) {
        this.pepperoni = A;
    }

    public void setNumberOfHam(int A) {
        this.ham = A;
    }

    public String getSize() {
        return this.size;
    }

    public int getNumberOfCheese() {
        return this.cheese;
    }

    public int getNumberOfPepperoni() {
        return this.pepperoni;
    }

    public int getNumberOfHam() {
        return this.ham;
    }

    public double calcCost() {
        double A = 0;
        if (this.size=="small") {
            A = 10;
        }
        if (this.size=="medium") {
            A = 12;
        }
        if (this.size == "large") {
            A = 14;
        }
        A = A + (this.cheese + this.ham + this.pepperoni) *2;
        return A;
    }

    public boolean equals(Pizza A) {
        if (this.size == A.size && this.cheese == A.cheese && this.ham == A.ham && this.pepperoni == A.pepperoni) {
            return true;
        } else {
            return false;
        }
    }

    public String toString(){
        String B;
        B="size = "+this.size+", numOfCheese = "+this.cheese+", numOfPepperoni = "+this.pepperoni+", numOfHam = "+this.ham;
        return B;
    }
}
