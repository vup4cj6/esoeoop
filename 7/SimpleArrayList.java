
/**
 * 整數列表
 */
public class SimpleArrayList {
    private java.lang.Integer[] A = new java.lang.Integer[0];
/**
 * 預設
 */
    public SimpleArrayList() {
    }
/**
 * 創建同時設定列表數字數量
 * 預設數字皆為0
 * @param n
 */
    public SimpleArrayList(int n) {
        this.A = new java.lang.Integer[n];
        for (int i = 0; i < n; i++) {
            this.A[i] = 0;
        }
    }
/**
 * 增加整數於列表最後方
 * @param i
 */
    public void add(java.lang.Integer i) {
        int l = this.A.length;
        java.lang.Integer[] B = new java.lang.Integer[l + 1];
        for (int c = 0; c < l; c++) {
            B[c] = this.A[c];
        }
        B[l] = i;
        this.A = B;
    }
/**
 * 取得列表中某項數值
 * 若項數不在列表範圍內則會得到null
 * @param i
 * @return
 */
    public java.lang.Integer get(int i) {
        if (i >= this.A.length || i < 0) {
            return null;
        }
        return this.A[i];
    }
/**
 * 更改某項數值同時取得原先數值
 * 若項數不在列表範圍內則會得到null
 * @param i
 * @param e
 * @return
 */
    public java.lang.Integer set(int i, java.lang.Integer e) {
        if (i >= this.A.length || i < 0) {
            return null;
        }
        java.lang.Integer t = this.A[i];
        this.A[i] = e;
        return t;
    }
/**
 * 移除某項數值並將後續數值前移一項
 * 若項數不在列表範圍內則會得到null
 */
    public boolean remove(int i) {
        int l = this.A.length;
        if (i > l || i < 0 || this.A[i] == null) {
            return false;
        }
        java.lang.Integer[] B = new java.lang.Integer[l - 1];
        for (int c = 0; c < i; c++) {
            B[c] = this.A[c];
        }
        for (int c = i; c < l - 1; c++) {
            B[c] = this.A[c + 1];
        }
        this.A = B;
        return true;
    }
/**
 * 清除列表
 */
    public void clear() {
        this.A = new java.lang.Integer[0];
    }
/**
 * 取得列表大小
 */
    public int size() {
        return this.A.length;
    }
/**
 * 移除前列表中並未出現在後列表中的數值
 * @param L
 * @return
 */
    public boolean retainAll(SimpleArrayList L) {

        int c = 0;
        for (int n = 0; n < this.A.length; n++) {
            int b = 0;
            for (int i = 0, l = L.A.length; i < l; i++) {
                int x = L.A[i];
                int y = this.A[n];
                if (x == y) {
                    b = 1;
                }
            }
            if (b == 0) {
                this.remove(n);
                c = 1;
                n--;
            }
        }
        if (c == 0) {
            return false;
        } else {
            return true;
        }
    }

}
