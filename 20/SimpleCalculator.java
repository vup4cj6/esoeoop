import java.text.DecimalFormat;

public class SimpleCalculator {
    private double result=0.00;
    private int count=0;
    private String operator="0";
    private String value="0";


    public void calResult(String cmd) throws UnknownCmdException{
        if(cmd.length()<3||cmd.charAt(1)!=' '){
            throw new UnknownCmdException(String.format("Please enter 1 operator and 1 value separated by 1 space"));
        }
        int a=1;
        String[] arr = cmd.split(" ");
        if (arr.length > 2) {
            throw new UnknownCmdException(String.format("Please enter 1 operator and 1 value separated by 1 space"));
        }
        String A=arr[0];
        String B=arr[1];
        if(A.equals("+")||A.equals("-")||A.equals("*")||A.equals("/")){
            a=0;
        }
        int b=0;
        for(int i=B.length();i>0;i--){
            if((B.charAt(i-1)<48||B.charAt(i-1)>57)&&B.charAt(i-1)!='.'){
                b=1;
            }
        }
        if(a==1&&b==1){
            throw new UnknownCmdException(A+" is an unknown operator and "+B+" is an unknown value");
        }
        if(a==1){
            throw new UnknownCmdException(A+" is an unknown operator");
        }
        if(b==1){
            throw new UnknownCmdException(B+" is an unknown value");
        }
        double i=Double.valueOf(B);
        int I=(int)i;
        if(A.equals("/")&&i==0){
            throw new UnknownCmdException("Can not divide by 0");
        }
        this.operator=A;
        this.value=B;
        
        if(A.equals("+")){
            this.result=this.result+i;
        }
        else if(A.equals("-")){
            this.result=this.result-i;
        }
        else if(A.equals("*")){
            this.result=this.result*i;
        }
        else if(A.equals("/")){
            this.result=this.result/i;
        }
    }
    

    public String getMsg(){
        String B=this.value;
        double i=Double.valueOf(B);
        DecimalFormat I=new DecimalFormat ("##0.00");
        String ii=I.format(i);
        String j=I.format(this.result);
        switch(this.count){
            case 0:
            this.count=1;
            return "Calculator is on. Result = "+j;
            case 1:
            this.count=2;
            return "Result "+this.operator+" "+ii+" = "+j+". New result = "+j;
            case 3:
            return "Final result = "+j;
            default:
            return "Result "+this.operator+" "+ii+" = "+j+". Updated result = "+j;
        }
        
    }
    public boolean endCalc(String cmd){
        if(cmd.equals("r")||cmd.equals("R")){
            this.count=3;
            return true;
        }
        return false;
    }

    public SimpleCalculator(){
        this.result=0.00;
        
    }
}